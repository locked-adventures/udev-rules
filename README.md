# Escape Room UDev Rules

## Usage
Add the file `99-serial.rules` to `/etc/udev/rules.d/` by typing the command `sudo cp -t /etc/udev/rules.d/ 99-serial.rules`.

Reload the udev rules with `sudo udevadm control --reload-rules && sudo udevadm trigger` (see [How to reload udev rules without reboot?](https://unix.stackexchange.com/questions/39370/how-to-reload-udev-rules-without-reboot)).

## Description
These udev rules create device symlinks which allow you to easily identify a serial device by its type (FTDI or CH) and USB port (e.g. 1.4). Symlink example: `/dev/ttyCH340:1.4`.
